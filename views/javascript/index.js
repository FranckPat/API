// Fonction de connexion  
  function envoiFormulaire(event){
    var login = document.querySelector('.login').value;
    var password = document.querySelector('.password').value;
    if(login === 'simplon' && password === '2018'){
    var ajax = $.ajax({
        url: '/API/login',
        method: 'POST',
        data :{
          login: login,
          passwd: password,
        }
      }).done(function (result){
          $("#main").hide();
          // alert("Attention aux images vous devez insérez vos url");
          $("#main2").show();
          afficheMain2();
      });
    }else{
      document.location.reload(true);
    }
  }

  // Affichage du main2
  function afficheMain2(){
    var main = document.querySelector('#main2');
    return main.style.visibility = 'visible';
  }

  // Fonction de déconnexion
  function Déconnexion(){
    localStorage.removeItem("token");
    $('#main2').hide();
    $('#main').show();
    document.location.reload(true);
    toastr.success('<b>Vous vous êtes déconnectez avec succès', 'Succès');
  }

  // Affichage Meetup en Ajax
  function afficheMeetUp(){
    $.ajax({
      url: '/API/meetups-allView',
      method: 'GET',
    }).done(function(element){
        var meetups = JSON.parse(element);
        $('.container').empty();
        if(meetups != ''){
        meetups.forEach(function(meetup) {
            $('.container').append(
              `
              <div class="meetup container-${meetup.id}">
                <div class="meetup_header">
                  <div class="left_header_meetup">
                    <h1>${meetup.title}</h1>
                  </div>
                  <div class="right_header_meetup">
                  <input type="button" class="button" value="Afficher" data-id="${meetup.id}" onclick ="affiche_meetup(this)">
                    <input type="image" class="modif" data-id="${meetup.id}" onclick="formUpdateMeetup(this)" src ="javascript/logoCrayon.png">
                    <input  type="image" src="javascript/logoCorbeille.jpg" value="submit" class="image_meetup" onclick="DeleteMeetup(${meetup.id})">
                  </div>
                </div>
                <div class="meetup_container">
                  <div class="meetup_section_description">
                      <h3>${meetup.description}</h3><br>
                  </div>
                  <div class="images">
                      <img class="images_meetup" src="${meetup.image}">
                  </div>
                </div>
                <div class="meetup_footer">
                  &copy;Tous droits réservé
              </div>
              `
            )
          })
        }else{
          alert("La base des meetups est vide");
          addMeetup();
        }

    });
  }

  // Affichage Location en Ajax
  function afficherLocation(){
      $.ajax({
        url: '/API/locations',
        method: 'GET',
      }).done(function (element){
          var locations = JSON.parse(element);
          $('.container').empty();
          locations.forEach(function(location) {
            $('.container').append(`<div class="locations">Adresse : ${location.address}<br>Ville : ${location.city}</div>`)
          })
      });
  }

  // Affichage speakers
  function afficherSpeaker(){
      $.ajax({
        url: '/API/speakers',
        method: 'GET',
      }).done(function (elementSpeakers) {
        var speakers = JSON.parse(elementSpeakers);
          $('.container').empty();
        speakers.forEach(function (speakers) {
            $('.container').append(
              `
                <div class="speakers">
                  <table class="tableau">
                    <tr class="colonne_Right">
                      <td>${speakers.id}</td>
                      <td>${speakers.first_name}</td>
                      <td>${speakers.last_name}</td>
                    </tr>
                  </table>      
                </div>
              `
            )
          })
      });
  }

//Afficher Subscribers
function afficherSubscribers() {
  $.ajax({
    url: '/API/subscribersAll',
    method: 'GET',
  }).done(function (element) {
    var subscribers = JSON.parse(element);
    $('.container').empty();
    subscribers.forEach(function (subscribers) {
      $('.container').append(`<div class="speakers">id : ${subscribers.id}<br>First name : ${subscribers.first_name} <br>Last name : ${subscribers.last_name} Email ${subscribers.mail_addr}</div>`)
    })
  });
}

// Ajouter un subscribers
function ajoutParticipant(){
  $('.container').empty()
  $('.container').append(
        `          
          <div class="Add_Subscribers">
              <input type="text" class = "First_Name" placeholder="First Name :" required>
              <input type="text" class = "Last_Name" placeholder= "Last Name :" required>
              <input type="mail" class = "Addresse_Email" placeholder="E-mail :" required>
              <input type="input" class="Addresse_Email" placeholder="Input" data-id="${meetup.id}" required>
              <input type="button" onclick="envoiSubscribers()" class="envoiSubscribers" value="Envoi de participants">
          </div>
        `
  )
}

//Envoi Subscribers
function envoiSubscribers(){
  var prenom = document.querySelector('.First_Name').value;
  var nom = document.querySelector('.Last_Name').value;
  var email = document.querySelector('.Addresse_Email').value;
  if (prenom != '' && nom != '' && email != ''){
    $.ajax({
      url: '/API/subscribers-ajout',
      method: 'POST',
      data:{
        first_name: prenom,
        last_name: nom,
        email: email
      }
    }).done(function (response){
        alert("Envoyez");
    });
  }else{
    alert('Veuillez remplir tous les champs');
  }
}

//Ajout Meetup
function addMeetup(){
  $('.container').empty()
  $('.container').append(
    `          
          <div class="Add_Meetup">
              <input type="text" class = "Title" placeholder="Title :" required>
              <input type="mail" class = "Images" placeholder="Images : (URL)" max-length="255" required>
              <textarea class="Description" placeholder="Description :"></textarea>
              <input type="button" onclick="envoiMeetup()" class="envoiMeetup" value="Envoi de meetup">
          </div>
        `
  )
}

//Envoi Meetupdataset
function envoiMeetup() {
  var title = document.querySelector('.Title');
  var image = document.querySelector('.Images');
  var description = document.querySelector('.Description');
  if (title.value != '' && image.value.length < 255 && description.value != '') {
    $.ajax({
      url: '/API/addMeetups',
      method: 'POST',
      data: {
        title: title.value,
        image: image.value,
        description: description.value
      }
    }).done(function (response) {
      alert("Envoyez");
      afficheMeetUp();
    });
    }else if(image.value.length > 255){
    alert("Veuillez saisir la bonne adresse d'URL");
  }else {
    alert('Veuillez remplir tous les champs');
  }
}

//Ajout Meetup
function addSpeaker() {
  $('.container').empty()
  $('.container').append(
    `          
          <div class="Add_Speaker">
              <input type="text" class = "First_name" placeholder="First_name :" required>
              <input type="text" class = "Last_Name" placeholder="Last_name :" required>
              <input type="button" onclick="envoiSpeaker()" class="envoiSpeaker" value="Envoi de speaker">
          </div>
        `
  )
}

//Ajout Speaker
function envoiSpeaker(){
  var Prenom = document.querySelector('.First_name').value;
  var Nom = document.querySelector('.Last_Name').value;
  if (Prenom != '' && Nom != ''){
    $.ajax({
      url: '/API/addSpeakers',
      method: 'POST',
      data: {
        prenom:Prenom,
        nom: Nom
      }
    }).done(function (response) {
      alert("Envoyez");
    });
  }else{
    alert("Dommage");
  }
}

//Modification MeetUp
function ModifMeetup(id,title,description,image){
  $('.container').empty();
  $('.container').append(
    `
      <div class="Updatemeetup">
        <input type="hidden" class="update__id" value = "${id}">
        <input type="text" class="update__title" placeholder ="Update title :">
        <input type="text" class="update__image" placeholder ="Update image :">
        <textarea type="text" class="update__description" placeholder ="Update description :">$</textarea>
        <input type="button" class="envoi" value = "Envoi" onclick = "UpdateMeetup()">
      </div>
    `);
}

function UpdateMeetup(button){
  var Id = document.querySelector('.update__id').value;
  var Title = document.querySelector('.update__title').value;
  var Image = document.querySelector('.update__image').value;
  var Description = document.querySelector('.update__description').value;
    if(Title != '' && Image != '' && Description != ''){
      $.ajax({
        url: '/API/meetups-update',
        method: 'POST',
        data: {
          updateTitle: Title,
          updateImage: Image,
          updateDescription: Description,
          id:Id,
        }
      }).done(function (response) {
        alert("Envoyez");
        afficheMeetUp();
      })
    }else{
      alert("\tVeuillez renseignez tout les \n champs vide s'il vous plaît\t");
    }
}

function DeleteMeetup(id) {
  $.ajax({
    url: '/API/meetups-delete/' + id,
    method: 'DELETE'
  }).done(function (data) {
    $('.container-' + id).remove();
  });
}

function formUpdateMeetup(button){
  var id = button.dataset.id;
  $.ajax({
    url: '/API/form-update/'+id,
    method: 'GET'
  }).done(function (response){
      console.log(response);
      var test = JSON.parse(response);
      console.log(test);
      var description = test[0].description;
      var id = test[0].id;
      var title = test[0].title;
      var image = test[0].image;
      console.log('description :' + ' ' + description);
      console.log('id :' + ' ' + id);
      console.log('title :' + ' ' + title);
      console.log('image :' + ' ' + image);
      ModifMeetup(id,description,title,image);
      jointures(id);
  })
}

function affiche_meetup(button) {
  var id = button.dataset.id
  $.ajax({
    url: '/API/addJointures/'+id,
    method: 'GET',
  }).done(function (response){
    console.log(response);
    var jointure = JSON.parse(response);
    var prenom = jointure[0].first_name;
    var nom = jointure[0].last_name;
    var email = jointure[0].mail_addr;
    var id = jointure[0].id;
    $('.container').empty();
    $('.container').append(
      `
        <div class="Participants">
          <div class="FirstNameParticipants">
            <h2>Prénom : ${prenom}</h2>
          </div>
          <div class="LastNameParticipants">
            <h2>Nom : ${nom}</h2>
          </div>
          <div class="MailParticipants">
            <h2>Adresse e-mail : ${email}</h2>
          </div>
        </div>
      `
    );
  });
}