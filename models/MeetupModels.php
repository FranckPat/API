<?php
require_once'DB.php';
    class MeetupModels{
        static function selection(){
            $db = BaseDonnee::connection();
            $result = $db->query('SELECT * FROM meetup');
            $resultat = $result->fetchAll();
            var_dump($resultat);
        }

        static function update($id,$Title,$Image,$Description){
            $bdd = BaseDonnee::connection();
            $req = $bdd->prepare('UPDATE meetup SET title=:txt,image=:image,description =:descriptions WHERE id = :id');
            $req->execute([
                'id'=>$id,
                'txt'=>$Title,
                'image'=>$Image,
                'descriptions'=>$Description,
            ]);
        }

         static function supression($id){
            $bdd = BaseDonnee::connection();
            $req = $bdd->prepare('DELETE FROM meetup WHERE id = :id');
            $req->execute([
                'id'=>$id
            ]);
        }
        static function view(){
            $bdd = BaseDonnee::connection();
            $req = $bdd->query('SELECT * FROM meetup');
            $res = $req->fetchAll(PDO::FETCH_ASSOC);
            return $res;
        }
          static function views($id){
            $bdd = BaseDonnee::connection();
            $text = 'Franck';
            $req = $bdd->query("SELECT * FROM meetup WHERE id = $id");
            $resultat = $req->fetchAll();
            var_dump($resultat);
        }
        static function ajout($title,$image,$description){
            $bdd = BaseDonnee::connection();
            $req = $bdd->prepare('INSERT INTO meetup(title, image, description) VALUES(:title,:image,:description)');
            $req->execute([
                'title' => $title,
                'image'=>$image,
                'description'=>$description
            ]);
        }
        function getMeetup($id){
            $db = BaseDonnee::connection();
            $result = $db->prepare('SELECT * FROM meetup WHERE id = :id');
            $result->execute([
                'id'=>$id
            ]);
            return $result->fetchAll(PDO::FETCH_ASSOC);
        }
    }
