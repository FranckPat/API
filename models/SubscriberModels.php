<?php
require_once 'DB.php';
    class Subscriber{
        static function ajout($firstname,$lastname,$mailaddr){
            $bdd = BaseDonnee::connection();
            $req = $bdd->prepare('INSERT INTO subscriber(first_name, last_name, mail_addr) VALUES(:first_name, :last_name, :mail_addr)');
            $req->execute([
                'first_name' => $firstname,
                'last_name'=>$lastname,
                'mail_addr'=>$mailaddr
            ]);
        }
        static function selectSubscribers(){
            $db = BaseDonnee::connection();
            $result = $db->query('SELECT * FROM subscriber');
            $resultat = $result->fetchAll(PDO::FETCH_ASSOC);
            return $resultat;
        }
    }