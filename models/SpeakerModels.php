<?php
    require_once'models/DB.php';
    class selection{
        static function selectSpeaker(){
            $db = BaseDonnee::connection();
            $result = $db->query('SELECT * FROM speaker');
            $resultat = $result->fetchAll(PDO::FETCH_ASSOC);
            return $resultat;
        }
        static function insertionSpeaker($prenom,$nom){
            $bdd = BaseDonnee::connection();
            $req = $bdd->prepare('INSERT INTO speaker (first_name,last_name) VALUES(:first_name,:last_name)');
            $req->execute([
                'first_name' => $prenom,
                'last_name'=>$nom,
            ]);
        }
        static function deleteSpeaker($id){
            $bdd = BaseDonnee::connection();
            $req = $bdd->prepare('DELETE FROM speaker WHERE id = :id');
            $req->execute([
                'id'=>$id
            ]);
        }
        static function update(){
            $bdd = BaseDonnee::connection();
            $first_name = 'Fred';
            $last_name = 'Skrillex';
            $description = 'Yeah une modif de faite';
            $req = $bdd->prepare('UPDATE speaker SET first_name =:first_name, last_name =:last_name, description = :description');
            $req->execute([
                'first_name'=>$first_name,
                'last_name'=>$last_name,
                'description'=>$description
            ]);
        }
    }
