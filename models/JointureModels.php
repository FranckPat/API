<?php
    // Dossier de connexion à la base de donnée
    require_once'models/DB.php';

    class JointureModels{
        static function afficheJointure($id){
            $bdd = BaseDonnee::connection();
            $resultat = $bdd->prepare('SELECT * FROM meetup as m 
            INNER JOIN meetup_subscriber as ms ON m.id = ms.id_meetup
            INNER JOIN subscriber as s ON s.id = ms.id_subscriber
            WHERE m.id =:id');
            $resultat->execute([
                'id'=>$id
            ]);
            return $resultat->fetchAll(PDO::FETCH_ASSOC);
        }
    }