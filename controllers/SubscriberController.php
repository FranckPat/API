<?php

require_once 'controllers/Controller.php';
require_once 'models/SubscriberModels.php';

class SubscriberController extends Controller {
    public function getAll() {
        $subscriber = new Subscriber();
        return json_encode($subscriber->getAll());
    }
    public function subscribersAll(){
        $subscribers = Subscriber::selectSubscribers();
            return json_encode($subscribers);
    }

    public function getWithId($id) {
        return json_encode('Mon id en param : ' . $id);
    }
    public function ajout(){
        $firstname = $_POST['first_name'];
        $lastname = $_POST['last_name'];
        $mailaddr = $_POST['email'];
        $addSubscriber = Subscriber::ajout($firstname,$lastname,$mailaddr);
        return json_encode($addSubscriber);
    }
    public function affichage() {
      Subscriber::affichage();
    }
}
