<?php
    require_once'models/SpeakerModels.php';
    class SpeakerController{
        public function getSpeaker(){
            $subscribers = selection::selectSpeaker();
            echo json_encode($subscribers);
            // return $subscribers;
        }
        public function deleteSpeakerWithId($id){
            selection::deleteSpeaker($id);
        }
        public function createSpeaker(){
            $prenom = $_POST['prenom'];
            $nom = $_POST['nom'];
            $addSpeaker = selection::insertionSpeaker($prenom,$nom);
            return json_encode($addSpeaker);
        }
        public function update(){
            selection::update();
        }
    }
?>
