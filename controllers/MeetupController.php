<?php
        require_once'models/MeetupModels.php';
    class MeetupController{
        public function getMeetup(){
            selectMeetup::selection();
        }

        public function createMeetup() {
            $id = $_POST['id'];
            $title = $_POST['title'];
            $image = $_POST['image'];
            $description = $_POST['description'];
            $addMeetup = MeetupModels::ajout($title,$image,$description,$id);
            return json_encode($addMeetup);
        }

        public function deleteMeetup($id) {
            $deleteMeetup = MeetupModels::supression($id);
            return json_encode($deleteMeetup);
        }
        public function viewMeetup(){
            $list = MeetupModels::view();
            return json_encode($list);
        }
        public function update(){
            $id = $_POST['id'];
            $Title = $_POST['updateTitle'];
            $Image = $_POST['updateImage'];
            $Description = $_POST['updateDescription'];
            $addUpdateMeetup = MeetupModels::update($id,$Title,$Image,$Description);
            return json_encode($addUpdateMeetup);
        }
        public function formUpdate($id){
            $list = MeetupModels::getMeetup($id);
            return json_encode($list);
        }
        public function viewMeetupWithId($id){
            MeetupModels::views($id);
        }
    }

?>
