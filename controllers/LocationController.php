<?php
    require_once'models/LocationModels.php';
    class LocationController{
        public function getLocation(){
          $list = select::selectLocation();
          return json_encode($list);
        }
        public function getLocations($id){
            Select::selectLocations($id);
        }


        public function supressions($id){
            Select::supression($id);
        }


        public function addLocation(){
            Select::addLocation();
        }
        public function updateLocation($id){
            Select::updateLocation($id);
        }
    }
