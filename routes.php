<?php
/**
 * Created by PhpStorm.
 * User: gchanteloube
 * Date: 12/02/18
 * Time: 22:38
 */

    use Pecee\SimpleRouter\SimpleRouter;
    require_once 'APIMiddleware.php';
    require_once 'controllers/LoginController.php';
    require_once 'controllers/DefaultController.php';
    require_once 'controllers/SubscriberController.php';
    require_once 'controllers/JointureController.php';
    // Require speaker meetup location et images
    require_once 'controllers/SpeakerController.php';
    require_once 'controllers/MeetupController.php';
    require_once 'controllers/LocationController.php';
    // on ajoute un préfixe car le site se trouve dans mon cas à l'adresse :
    // http://localhost/simplon/routeur/

    $prefix = '/API';

    SimpleRouter::group(['prefix' => $prefix], function () {
    SimpleRouter::post('/login', 'LoginController@login');
    SimpleRouter::get('/login', 'LoginController@error')->name('login');
    SimpleRouter::get('/', 'DefaultController@defaultAction');

    // ROUTE SUBSCRIBERS

    SimpleRouter::get('/subscribers', 'SubscriberController@getAll')->addMiddleware(APIMiddleware::class);
    SimpleRouter::get('/subscribersAll', 'SubscriberController@subscribersAll');
    SimpleRouter::get('/subscribers/{id}', 'SubscriberController@getWithId');
    SimpleRouter::post('/subscribers-ajout', 'SubscriberController@ajout');
    SimpleRouter::get('/speakers-delete/{id}', 'SpeakerController@deleteSpeakerWithId'); //fait
    SimpleRouter::post('/subscribers/{id}', 'SubscriberController@getWithId');

    // ROUTE MEETUP

    SimpleRouter::get('/meetups', 'MeetupsController@getMeetup');// fait
    SimpleRouter::post('/addMeetups', 'MeetupController@createMeetup');// fait
    SimpleRouter::delete('/meetups-delete/{id}', 'MeetupController@deleteMeetup');// fait
    SimpleRouter::get('/meetups-allView', 'MeetupController@viewMeetup');// fait
    SimpleRouter::get('/meetups-view', 'MeetupController@viewMeetupWithId');// fait
    SimpleRouter::post('/meetups-update', 'MeetupController@update');// fait
    SimpleRouter::get('/form-update/{id}', 'MeetupController@formUpdate');// fait

    // ROUTE LOCATION

    SimpleRouter::get('/locations', 'LocationController@getLocation'); //fait
    SimpleRouter::get('/add-locations', 'LocationController@addLocation'); //fait
    SimpleRouter::get('/locations-delete/{id}', 'LocationController@supressions'); // fait
    SimpleRouter::get('/locations/{id}', 'LocationController@getLocations'); // fait
    SimpleRouter::get('/update-locations/{id}', 'LocationController@updateLocation'); // fait

    // SPEAKERS

    SimpleRouter::get('/speakers', 'SpeakerController@getSpeaker');//fait
    SimpleRouter::get('/speakers-update', 'SpeakerController@update'); //fait
    SimpleRouter::post('/addSpeakers', 'SpeakerController@createSpeaker'); // fait

    // JOINTURES
    SimpleRouter::get('/addJointures/{id}', 'JointureController@afficheJointure'); // fait

});
